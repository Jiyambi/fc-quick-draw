﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; // Needed for SFX
using Microsoft.Xna.Framework.Media; // Needed for music (Song)
using System;

namespace QuickDraw
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Loaded in game assets
        Texture2D buttonTexture;
        SpriteFont gameFont;
        SoundEffect clickSFX;
        SoundEffect startSignalSFX;
        Song gameMusic;

        // Game state / input
        MouseState previousState;
        bool playing = false;
        float timeUntilSignal = 0f;
        float timeSinceSignal = 0f;
        Random rand = new Random();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            // Load button graphic
            buttonTexture = Content.Load<Texture2D>("graphics/button");

            // Load game font
            gameFont = Content.Load<SpriteFont>("fonts/mainSpriteFont");

            // Load SFX
            clickSFX = Content.Load<SoundEffect>("audio/buttonClick");
            startSignalSFX = Content.Load<SoundEffect>("audio/gameOver");

            // Load Music
            gameMusic = Content.Load<Song>("audio/music");

            // Start background music
            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;

            // Make it so we can see the mouse
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // Get current mouse state
            MouseState currentState = Mouse.GetState();

            // Find the centre of the screen
            Vector2 screenCentre = new Vector2(
                Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2
            );

            // Determine button rectangle
            Rectangle buttonRect = new Rectangle(
                    (int)screenCentre.X - buttonTexture.Width / 2,
                    (int)screenCentre.Y - buttonTexture.Height / 2,
                    buttonTexture.Width,
                    buttonTexture.Height);


            // Check if we have clicked the mouse
            if (currentState.LeftButton == ButtonState.Pressed
                && previousState.LeftButton != ButtonState.Pressed
                && buttonRect.Contains(currentState.X, currentState.Y))
            {
                // Mouse is pressed!
                clickSFX.Play();

                // End the game!
                if (playing == true)
                {
                    playing = false;

                    // Draw will determine what to do based on this!
                }
                else // if playing is false
                {
                    // We weren't playing yet, and we now should start playing

                    // Set playing to true
                    playing = true;

                    // Set our timer to a random number
                    timeUntilSignal = (float)rand.Next(3, 11);
                    timeSinceSignal = 0f;
                }

            }

            if (playing == true)
            {
                // Update our time remaining
                // Subtract the time passed this frame from our time remaining
                timeUntilSignal -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (timeUntilSignal <= 0)
                    timeSinceSignal = timeUntilSignal * -1.0f;
            }

            // Current state becomes previous state
            previousState = currentState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            // Start drawing
            spriteBatch.Begin();

            // Find the centre of the screen
            Vector2 screenCentre = new Vector2(
                Window.ClientBounds.Width / 2,
                Window.ClientBounds.Height / 2
            );

            // Draw button
            Color buttonColor = Color.White;
            if (playing == true && timeUntilSignal <= 0)
                buttonColor = Color.Red;
            spriteBatch.Draw(
                buttonTexture,
                new Rectangle(
                    (int)screenCentre.X - buttonTexture.Width / 2,
                    (int)screenCentre.Y - buttonTexture.Height / 2,
                    buttonTexture.Width,
                    buttonTexture.Height),
                buttonColor
            );


            // Draw Text
            Vector2 titleSize = gameFont.MeasureString("Quick Draw");

            spriteBatch.DrawString(
                gameFont,
                "Quick Draw",
                screenCentre - new Vector2(0, 100) - titleSize / 2,
                Color.White
            );

            Vector2 authorSize = gameFont.MeasureString("by Sarah Herzog");

            spriteBatch.DrawString(
                gameFont,
                "by Sarah Herzog",
                screenCentre - new Vector2(0, 75) - authorSize / 2,
                Color.White
            );


            string promptString = "Click the button to start!";
            if (playing == true && timeUntilSignal > 0)
                promptString = "Wait for the signal!";
            else if (playing == true && timeSinceSignal > 0)
                promptString = "CLICK!";
            else if (playing == false && timeUntilSignal > 0)
                promptString = "Too soon!";
            else if (playing == false && timeSinceSignal > 0)
                promptString = "Time: "+(int)(timeSinceSignal * 1000)+" ms";
            Vector2 promptSize = gameFont.MeasureString(promptString);

            spriteBatch.DrawString(
                gameFont,
                promptString,
                screenCentre - new Vector2(0, 50) - promptSize / 2,
                Color.White
            );

            // Stop drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
